/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import com.sun.prism.paint.Color;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTextField;

import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *
 * @author ftonye
 */
public class BattleSea extends JFrame
{
    private static JButton[] listbtn;
    private static ArrayList<Ship> listShip;
    private static ArrayList<JButton> listJButtonInShip;
    
    
    private InetAddress adrs;
    
    private JButton btnStartServer;
    private JTextField txtPortNumber;
    private BackGroundCom com;
    private Thread t;
    private static JPanel frame;
    public static  JPanel feu;
    private JRadioButton chooseserver;
    private static Boolean over;
   
    
    public BattleSea()
    {
        this.setSize(620,470);
        
        try {
            adrs = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(BattleSea.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        this.setTitle("BattleSea Server address is: " + adrs.getHostAddress());
        this.setLayout(new FlowLayout());
        
        createGrid();
        linkListenerToSeaSector();
           
        this.add(feu);
        this.add(chooseserver);
        this.add(btnStartServer);
        this.add(txtPortNumber);
        
        
         CreateShips();
        over=false;
    
       
        
    }
    
    
    private void createGrid()
    {
        listbtn = new JButton[100];
        frame=new JPanel();
        frame.setLayout(new GridLayout(10,10));
        
        for(int i = 0; i< listbtn.length;i++)
        {
         listbtn[i] = new JButton(String.valueOf(i)); 
         listbtn[i].setSize(10,10);
         listbtn[i].setBackground(java.awt.Color.blue); 
         listbtn[i].setForeground(java.awt.Color.blue);  
         listbtn[i].setFont(new Font("Arial", Font.BOLD, 24));
         //listbtn[i].setBorder(null);
        frame.add(listbtn[i]);
        
        }
        this.getContentPane().add(frame);
        this.feu=new JPanel();
        
        
        
        this.chooseserver =new  JRadioButton();
      
      
        
        btnStartServer = new JButton("Start");
        btnStartServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
               
                if(chooseserver.isSelected()){
               com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()),true);
               feu.setBackground(java.awt.Color.green);
                }
                else{
                com = new BackGroundCom(Integer.parseInt(txtPortNumber.getText().toString()),false);
                feu.setBackground(java.awt.Color.red);
                }
                
              t = new Thread(com);
               t.start();
            }
        
        });
        txtPortNumber = new JTextField();
        txtPortNumber.setText("Enter Port number");
    }
    
    
    private static Boolean allDown(){
        
          for(Ship element:listShip)
        {
            
            if(element.getStatus()==0){
                
                return false;
            }
            
        }
        
          JOptionPane.showMessageDialog(null,"You Lose !");
        return true;
    }
    
    private void linkListenerToSeaSector()
    {
        for(int i = 0; i<listbtn.length;i++)
        {
             listbtn[i].addActionListener(new ActionListener()
          {
             @Override
             public void actionPerformed(ActionEvent ae) 
             {
               
                
              
                com.case_cible = Integer.parseInt(ae.getActionCommand());
                 JButton source = (JButton) ae.getSource();
                
                if(com.turn&&source.getForeground()==java.awt.Color.blue){
                com.dataToSend = true;
                }
                
                 
                  
             }    
          });
        }
         
    }
    
    private Boolean checkcolision(int head,int nb,int h){
        
        for(int i=head;i<head+nb;i++){
        for(JButton element:listJButtonInShip)
        {
            
            if((h==1 && ( Integer.parseInt(element.getText())==i || Integer.parseInt(element.getText())==i+1 ||Integer.parseInt(element.getText())==i-1 ||Integer.parseInt(element.getText())==i+10 ||Integer.parseInt(element.getText())==i-10 ))||(h==0&&(Integer.parseInt(element.getText())==head+10*(i-head) || Integer.parseInt(element.getText())==head+10*(i-head)-10||Integer.parseInt(element.getText())==head+10*(i-head)+10|| Integer.parseInt(element.getText())==head+10*(i-head)+1 ||Integer.parseInt(element.getText())==head+10*(i-head)-1 )))
                return false;
            
        }
        
    }
        
        return true;
        
    }
       
    private void CreateShips()
    {
       
        int head ,nb = 0;
        Random r = new Random();
        listShip = new ArrayList<Ship>();
        listJButtonInShip = new ArrayList<JButton>();
        
        
    
        for(int i = 0;i<5;i++)
        {
                 int h = r.nextInt(2);
                 Boolean verif=false;
                 
                 ArrayList<JButton> boatsection = new ArrayList<JButton>();
                 head=0;
                 
            
                 while(!verif){
                 head = r.nextInt(98);
                 nb = r.nextInt(4)+2;
                 
                 if(h==1&&(head+nb)<99&&head%10<(head+nb)%10&&checkcolision(head,nb,h) || h==0&&(head+nb*10)<99 && head%10==(head+nb*10)%10 && checkcolision(head,nb,h)  )
                  verif=true;       
                 
                 
                 }
                
                 for(int j=0;j< nb ;j++)
                 {
                     if(h==1){
                   boatsection.add(listbtn[head+j]);
                   listJButtonInShip.add(listbtn[head+j]);
                     }
                     else{
                        boatsection.add(listbtn[head+j*10]);
                   listJButtonInShip.add(listbtn[head+j*10]); 
                     }
                 }
                 
                 Ship s = new Ship(boatsection);
                 listShip.add(s);
                 
           
           
         
            
        }
        
        
        
    }
    
    
    public static void ship_down(int case_cible){
        
        
        listbtn[case_cible].setForeground(java.awt.Color.black);
        
        
        if(case_cible+1<=99 && listbtn[case_cible+1].getForeground()==java.awt.Color.red)
          ship_down(case_cible+1);  
        if(case_cible-1>=0&&listbtn[case_cible-1].getForeground()==java.awt.Color.red)
          ship_down(case_cible-1);  
         if(case_cible+10<=99&&listbtn[case_cible+10].getForeground()==java.awt.Color.red)
           ship_down(case_cible+10);  
         if(case_cible-10>=0&&listbtn[case_cible-10].getForeground()==java.awt.Color.red)
           ship_down(case_cible-10);
   }
        
        
    
    
    public static int UpdateGrig(int incomming,int case_cible,int status)
    {
        Boolean missHit = true;
        
        
        if(status==1)
            listbtn[case_cible].setForeground(java.awt.Color.red);
        else if(status==2)
            ship_down(case_cible);
                  
        else if(status==0)
            listbtn[case_cible].setForeground(java.awt.Color.magenta); 
        else if(status==3&&!over){
           ship_down(case_cible);
           JOptionPane.showMessageDialog(null,"You Win !");
           over=true;
           
        }
        
        
        
        
           
                  int ship_statut=0;
                  for(Ship element:listShip)
                  {
                      int temp=element.checkHit(listbtn[incomming]);
                      if(temp>0)
                      ship_statut=temp;
                     
                  }  
                 
                  
                
          
        
        
        for(JButton element:listJButtonInShip)
        {
            if(element.equals(listbtn[incomming])){
                missHit = false;
                 
            }
        }
     
         if(missHit)
         {
             listbtn[incomming].setBackground(java.awt.Color.magenta); 
             
             
         }
         
         if(allDown()&&!over){
         ship_statut=3;
         over=true;
         }
        
         
         return ship_statut;
    }
    
    public static void end_game(String message){
        
       frame.removeAll();
       JLabel jlabel = new JLabel(message);
       jlabel.setFont(new Font("Verdana",1,20));
        frame.add(jlabel);
        
        
    }
    
}
