/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import javax.swing.JOptionPane;
import java.io.IOException;
import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ftonye
 */
public class BackGroundCom implements Runnable
{
     private int port;
     private ServerSocket ss ;
     private Socket cs;
     private Scanner reader;
     private PrintStream writer ;
     
     public int case_cible;
     public int case_retour;
     public int status;
     
     private String MsgGot;
     private String MsgSent;
     public Boolean dataToSend;
     private Boolean type;
     public Boolean turn;
     InetAddress sa = null;
    
     
     public BackGroundCom(int port,Boolean type)
     {
         this.port = port;
         dataToSend = false;
         this.type=type;
         case_cible = 100;
         case_retour=100;
         status=100;
         startServer();
         
     }
     
     private void startServer()
     {
        try 
        {   
            if(this.type){
                sa = InetAddress.getByName("192.168.0.145");
            ss = new ServerSocket(this.port,100,sa);
            cs = ss.accept();
             JOptionPane.showMessageDialog(null,"Server accept connection from" + cs.getInetAddress().getHostAddress());
             turn=true;
            }
            else{
                 cs = new Socket("192.168.0.145",this.port);
                 turn=false;
            }
           
        }
        catch (IOException ex)
        {
          JOptionPane.showMessageDialog(null,ex.getMessage());
        }
					 
	System.out.println("Server accept connection");
     }
     
     private void decrypt(){
         
        case_retour= Integer.parseInt(MsgGot.split(",")[0]);
         case_cible= Integer.parseInt(MsgGot.split(",")[1]);
         status= Integer.parseInt(MsgGot.split(",")[2]);
     }
     
      private void encrypt(){
         
        MsgSent="";
        MsgSent=String.valueOf(case_cible)+","+String.valueOf(case_retour)+","+String.valueOf(status);
        
     }
     
    @Override
    public void run()
    {
        
        
        try
        {
           reader = new Scanner(cs.getInputStream());
        } 
          catch (IOException ex)
        {
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
          
        try
        {
           writer = new PrintStream(cs.getOutputStream());
        } 
        catch (IOException ex) 
        {
            JOptionPane.showMessageDialog(null,ex.getMessage());
        }
        while(true) 
        {
            
           synchronized(dataToSend)
           {
                if(dataToSend||status==3)
                {
                     encrypt();
                     writer.println(MsgSent); 
                     dataToSend = false;
                     turn=false;
                     BattleSea.feu.setBackground(java.awt.Color.red);
                     
                } 
                else
                {
                    writer.println("n/a");
                }
           };
           
           
           
           MsgGot = reader.nextLine();
           if(!MsgGot.contains("n/a"))
           {
               decrypt();
               turn=true;
               BattleSea.feu.setBackground(java.awt.Color.green);
               status=BattleSea.UpdateGrig(case_retour,case_cible,status);
               
               
           }
           

          
             
             
        }
      
       
    }

    
}
