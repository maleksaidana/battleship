/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import java.util.ArrayList;
import javax.swing.JButton;

/**
 *
 * @author ftonye
 */
public class Ship 
{
    public ArrayList<JButton> BoatSections;
  
    private int status;
    private int nbHit;
    
    public Ship(ArrayList<JButton> BoatSection)
    {
        this.BoatSections = new ArrayList<JButton>();
        this.BoatSections = BoatSection;
        this.status = 0;
        this.nbHit = 0;
        initShip();
    }
    
    
   public int getStatus()
   {
       return this.status;
   }
   
   public void initShip()
   {
       
       for(JButton section:this.BoatSections)
       {
          
            section.setBackground(java.awt.Color.green);
           
         
        }
               
    }
  
   public  int checkHit(JButton cible)
   {
      int stat=0;
      Boolean verif=false;
       for(JButton section:this.BoatSections)
       {
           if (section.equals(cible))
           {
             verif=true;
             this.nbHit++;
             section.setBackground(java.awt.Color.red);
             stat=1;
                         
           }
          
               
       }
       
       if(verif&&this.nbHit==this.BoatSections.size()){
           this.status=1;
           stat=2;
           for(JButton section:this.BoatSections){
               section.setBackground(java.awt.Color.black);
               
           }
       
       }   
     return stat;
   }     
}
